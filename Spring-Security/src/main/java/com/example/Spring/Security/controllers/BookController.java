package com.example.Spring.Security.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/book")
public class BookController {
    @GetMapping("/index")
    public ModelAndView getBook() {
        ModelAndView view = new ModelAndView("/book/index");
        return view;
    }
}
