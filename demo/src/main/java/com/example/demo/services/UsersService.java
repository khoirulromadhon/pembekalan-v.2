package com.example.demo.services;

import com.example.demo.models.Users;

import java.util.List;

public interface UsersService {
    List<Users> getAllUsers();
}
