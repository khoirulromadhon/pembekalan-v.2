package com.example.demo.services.impl;

import com.example.demo.models.Users;
import com.example.demo.repositories.UsersRepositories;
import com.example.demo.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImplUsersService implements UsersService {
    @Autowired
    UsersRepositories usersRepositories;

    @Override
    public List<Users> getAllUsers() {
        return usersRepositories.findAll();
    }
}
