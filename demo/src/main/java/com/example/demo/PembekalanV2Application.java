package com.example.demo;

import com.example.demo.models.Users;
import com.example.demo.repositories.UsersRepositories;
import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Locale;

@SpringBootApplication
public class PembekalanV2Application {
	@Autowired
	UsersRepositories usersRepositories;

	public static void main(String[] args) {
		SpringApplication.run(PembekalanV2Application.class, args);
	}

	@Bean
	CommandLineRunner commandLineRunner() {

		BCryptPasswordEncoder bcyript = new BCryptPasswordEncoder();
		Faker faker = new Faker(new Locale("en-IND"));
		return args -> {
//			Users users = new Users("Jumadi Kopling", "jumadi@mail.com", bcyript.encode("123456"));
//			usersRepositories.save(users);

			for (int i = 0; i < 10; i++) {
				Users users = new Users(faker.name().fullName(), faker.internet().safeEmailAddress(), bcyript.encode("123456"));
				usersRepositories.save(users);
			}
		};
	}

}
