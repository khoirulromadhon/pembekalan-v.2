package com.example.demo.controllers.rest;

import com.example.demo.DTO.UsersResponseDTO;
import com.example.demo.models.Users;
import com.example.demo.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/users")
public class UsersController {
    @Autowired
    UsersService usersService;

    @GetMapping("/")
    public ResponseEntity<?> getAllUsers(){
        List<UsersResponseDTO> usersResponseDTOList = new ArrayList<>();
        List<Users> users = usersService.getAllUsers();
        Map<String, Object> result = new HashMap<>();

        try {
            for (int i = 0; i < users.size(); i++) {
                UsersResponseDTO usersResponseDTO = new UsersResponseDTO();

                usersResponseDTO.setName(users.get(i).getUser_name());
                usersResponseDTO.setEmail(users.get(i).getEmail());

                usersResponseDTOList.add(usersResponseDTO);
            }

            result.put("status", "200");
            result.put("Message", "Data Success Acuired");
            result.put("Data", usersResponseDTOList);

            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        catch (Exception exception){
            result.put("Status", "500");
            result.put("Message", exception.getMessage());
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
